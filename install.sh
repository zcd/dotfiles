#!/usr/bin/env bash

set -eux -o pipefail

BACKUP=${PWD}/$(date '+%Y%m%d%H%M%S').bk.d
DATA=${PWD}/data

if [[ $EUID -eq 0 ]]
then
  echo "Current user is root. Probably don't want to do that."
  exit 1
fi

# Set up dotfiles
for item in $(ls --almost-all $DATA)
do
  src=${DATA}/$item
  dst=${HOME}/$item

  if [ -L $src ]
  then
    echo "unexpected symlink source $src"
    exit 1
  fi

  if [ -L $dst ]
  then
    echo "no back up $dst since it's just a symlink to $(readlink $dst)"
    rm $dst
  elif [[ -d $dst || -f $dst ]]
  then
    mkdir -p $BACKUP
    mv $dst $BACKUP/
  fi

  ln --symbolic --target-directory=${HOME} $src
done
